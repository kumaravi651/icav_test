import 'package:flutter_test/flutter_test.dart';
import 'package:icavtech/controllers/login_controllers.dart';

void main() {
  test('empty email returns error string', () {
    var result = LoginController();
    var testEmail = result.validateEmail('');
    expect(testEmail, 'Provide valid Email');
  });

  test('non-empty email return null', () {
    var result = LoginController();
    var testEmail = result.validateEmail('pass');
    expect(testEmail, 'Provide valid Email');
  });

  test('empty Password returns error string', () {
    var result = LoginController();
    var testPassword = result.validatePassword('');
    expect(testPassword, 'Password must be of 6 characters');
  });

  test('Password must be of 6 characters', () {
    var result = LoginController();
    var testPassword = result.validatePassword('psss');
    expect(testPassword, 'Password must be of 6 characters');
  });
}
