import 'package:dio/dio.dart';
import 'package:test/test.dart';
import 'package:nock/nock.dart';

void main() {
  setUpAll(() {
    nock.init();
  });

  setUp(() {
    nock.cleanAll();
  });

  test('Api Test Cases', () async {
    final client = Dio();
    final interceptor = nock('https://newsapi.org//v2').get(
        '/top-headlines?country=us&category=general&apiKey=85a2c98d226a4365ac892854e30287d7')
      ..reply(
        200,
        'articles',
      );

    final response = await client.get(
        'https://newsapi.org//v2/top-headlines?country=us&category=general&apiKey=85a2c98d226a4365ac892854e30287d7');

    expect(interceptor.isDone, true);
    expect(response.statusCode, 200);
    expect(response.data, 'articles');
  });
}
