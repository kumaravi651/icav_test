import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/helper/shared_preference.dart';
import 'package:icavtech/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SessionManager status = SessionManager();
  var check = await status.getIsLogin();

  runApp(check == true ? HomeApp() : MyApp());
}

class HomeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      initialRoute: AppPages.HOMESCREEN,
      getPages: AppPages.routes,
    );
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      initialRoute: AppPages.LOGIN,
      getPages: AppPages.routes,
    );
  }
}
