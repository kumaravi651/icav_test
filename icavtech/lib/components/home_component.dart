import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/controllers/home_controller.dart';
import 'package:icavtech/widgets/article_card.dart';
import 'package:icavtech/widgets/launch_url.dart';

class HomeComponent extends GetView<HomeController> {
  final HomeController homeController;
  HomeComponent({@required this.homeController});
  @override
  Widget build(BuildContext context) {
    final HomeController _homeController = Get.find();

    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 3),
        child: Container(
          child: Obx(() {
            if (_homeController.isLoading.value)
              return const Center(
                child: const CircularProgressIndicator(),
              );
            else
              return ListView.builder(
                itemCount: _homeController.newsList.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () =>
                        launchURL(context, _homeController.newsList[index].url),
                    child: ArticleCard(
                      imageUrl: _homeController.newsList[index].urlToImage,
                      title: _homeController.newsList[index].title,
                      source: _homeController.newsList[index].source.name,
                      publishedAt: _homeController.newsList[index].publishedAt,
                    ),
                  );
                },
              );
          }),
        ));
  }
}
