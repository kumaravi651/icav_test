import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/constants/icav_constants.dart';
import 'package:icavtech/constants/size_config.dart';
import 'package:icavtech/controllers/login_controllers.dart';
import 'package:icavtech/database/database_helper.dart';
import 'package:icavtech/helper/shared_preference.dart';
import 'package:icavtech/routes/app_routes.dart';
import 'package:icavtech/widgets/icav_txt.dart';

import 'package:icavtech/widgets/rounded_elevated_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginComponent extends StatefulWidget {
  final LoginController loginController;
  LoginComponent({@required this.loginController});

  @override
  _LoginComponentState createState() => _LoginComponentState();
}

class _LoginComponentState extends State<LoginComponent> {
  final dbHelper = DatabaseHelper.instance;

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      left: true,
      right: true,
      child: Container(
        margin: EdgeInsets.only(top: 60, left: 16, right: 16),
        width: Get.width,
        height: context.height,
        child: SingleChildScrollView(
          child: Form(
            key: widget.loginController.loginFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                ICAVText(
                  title: 'Login',
                  weight: FONTWEIGHT.medium,
                  device: SizeConfig.devicetype,
                  type: "h2",
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Email",
                    prefixIcon: Icon(Icons.email),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  controller: widget.loginController.emailController,
                  onSaved: (value) {
                    widget.loginController.email = value;
                  },
                  validator: (value) {
                    return widget.loginController.validateEmail(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Password",
                    prefixIcon: Icon(Icons.lock),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  controller: widget.loginController.passwordController,
                  onSaved: (value) {
                    widget.loginController.password = value;
                  },
                  validator: (value) {
                    return widget.loginController.validatePassword(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                ConstrainedBox(
                    constraints: BoxConstraints.tightFor(width: context.width),
                    child: RoundedElevatedButton(
                        width: double.infinity,
                        onPressed: () async {
                          widget.loginController.checkLogin();
                          final form =
                              widget.loginController.loginFormKey.currentState;

                          if (form.validate()) {
                            form.save();
                            _insert();
                            Navigator.pushNamed(context, Routes.HOME);
                            SessionManager prefs = SessionManager();
                            prefs.setIsLogin(true);
                            FocusScope.of(context).unfocus();
                            widget.loginController.loginFormKey.currentState
                                .reset();
                          }
                        },
                        childText: 'Login')),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Does not have account?'),
                      FlatButton(
                        textColor: Colors.blue,
                        child: Text(
                          'Sign up',
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          //signup screen
                          Navigator.pushNamed(context, Routes.SIGNUP);
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _insert() async {
    Map<String, dynamic> row = {
      DatabaseHelper.columnPass: widget.loginController.password,
      DatabaseHelper.columnEmail: widget.loginController.email,
    };

    final id = await dbHelper.insert(row);
    print("Id is:   $id");
  }
}
