import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/constants/icav_constants.dart';
import 'package:icavtech/constants/size_config.dart';
import 'package:icavtech/controllers/sign_up_controllers.dart';
import 'package:icavtech/routes/app_routes.dart';
import 'package:icavtech/widgets/icav_txt.dart';
import 'package:icavtech/widgets/rounded_elevated_button.dart';

class SignUpComponent extends StatefulWidget {
  final SignUpController signUpController;
  SignUpComponent({@required this.signUpController});

  @override
  _SignUpComponentState createState() => _SignUpComponentState();
}

class _SignUpComponentState extends State<SignUpComponent> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: true,
      left: true,
      right: true,
      child: Container(
        margin: EdgeInsets.only(top: 60, left: 16, right: 16),
        width: Get.width,
        height: context.height,
        child: SingleChildScrollView(
          child: Form(
            key: widget.signUpController.signUpFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                ICAVText(
                  title: 'Sign Up',
                  weight: FONTWEIGHT.medium,
                  device: SizeConfig.devicetype,
                  type: "h2",
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "First Name",
                    prefixIcon: Icon(Icons.nat),
                  ),
                  keyboardType: TextInputType.text,
                  controller: widget.signUpController.firstNameController,
                  onSaved: (value) {
                    widget.signUpController.firstName = value;
                  },
                  validator: (value) {
                    return widget.signUpController.validateFirstName(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Last Name",
                    prefixIcon: Icon(Icons.last_page),
                  ),
                  keyboardType: TextInputType.text,
                  controller: widget.signUpController.lastNameController,
                  onSaved: (value) {
                    widget.signUpController.lastName = value;
                  },
                  validator: (value) {
                    return widget.signUpController.validateLastName(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Email",
                    prefixIcon: Icon(Icons.email),
                  ),
                  keyboardType: TextInputType.emailAddress,
                  controller: widget.signUpController.emailController,
                  onSaved: (value) {
                    widget.signUpController.email = value;
                  },
                  validator: (value) {
                    return widget.signUpController.validateEmail(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    labelText: "Password",
                    prefixIcon: Icon(Icons.lock),
                  ),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                  controller: widget.signUpController.passwordController,
                  onSaved: (value) {
                    widget.signUpController.password = value;
                  },
                  validator: (value) {
                    return widget.signUpController.validatePassword(value);
                  },
                ),
                SizedBox(
                  height: 16,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Already a member?'),
                      FlatButton(
                        textColor: Colors.blue,
                        child: Text(
                          'Sign in',
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          //signup screen
                          //Navigator.pushNamed(context, Routes.SIGNUP);
                          Navigator.pop(context);
                        },
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                ConstrainedBox(
                  constraints: BoxConstraints.tightFor(width: context.width),
                  child: RoundedElevatedButton(
                    width: double.infinity,
                    onPressed: () {
                      widget.signUpController.checkSignUp();
                      final form =
                          widget.signUpController.signUpFormKey.currentState;

                      if (form.validate()) {
                        form.save();
                        Navigator.pushNamed(context, Routes.HOME);
                        FocusScope.of(context).unfocus();
                        widget.signUpController.signUpFormKey.currentState
                            .reset();
                      }
                    },
                    childText: 'Sign Up',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
