import 'package:get/get.dart';
import 'package:icavtech/controllers/home_controller.dart';

class HomeBinding extends Bindings {
  void dependencies() {
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
