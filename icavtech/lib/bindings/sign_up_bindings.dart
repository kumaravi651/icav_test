import 'package:get/get.dart';
import 'package:icavtech/controllers/sign_up_controllers.dart';

class SignUpBindings extends Bindings {
  void dependencies() {
    Get.lazyPut<SignUpController>(
      () => SignUpController(),
    );
  }
}
