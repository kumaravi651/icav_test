import 'package:get/get.dart';
import 'package:icavtech/controllers/login_controllers.dart';

class LoginBindings extends Bindings {
  void dependencies() {
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
  }
}
