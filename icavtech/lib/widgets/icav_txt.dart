import 'package:flutter/material.dart';
import 'package:icavtech/constants/icav_constants.dart';
import 'package:icavtech/constants/size_config.dart';

class ICAVText extends StatelessWidget {
  final String title;
  final String type;
  final Color color;
  final DEVICETYPE device;
  final FONTWEIGHT weight;
  final bool isOverflow;
  final bool isMaxLines;
  final int maxLine;
  final TextDecoration textDecoration;

  ICAVText(
      {Key key,
      this.title,
      this.type = "B1",
      this.isOverflow = false,
      this.color = kLabelTextColor,
      this.device = DEVICETYPE.MOBILE,
      this.weight = FONTWEIGHT.medium,
      this.isMaxLines = false,
      this.maxLine = 3,
      this.textDecoration})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return Text(
      title,
      overflow: isOverflow ? TextOverflow.ellipsis : null,
      textScaleFactor: 1.0,
      maxLines: isMaxLines ? maxLine : null,
      style: TextStyle(
        color: color,
        fontFamily: "JioType",
        fontSize: SizeConfig.devicetype.typescale[type],
        fontWeight: weight.value,
        decoration: textDecoration ?? TextDecoration.none,
      ),
    );
  }
}
