import 'package:get/get.dart';
import 'package:icavtech/services/news_services.dart';

class HomeController extends GetxController {
  var newsList = [].obs;
  var isLoading = true.obs;
  var category = "general".obs;

  @override
  void onInit() {
    fetchTopHeadlines(category);

    super.onInit();
  }

  void fetchTopHeadlines(RxString category) async {
    try {
      isLoading(true);
      update();
      var articles = await RemoteServices.fetchTopHeadlines(category.string);

      if (articles != null) {
        newsList.assignAll(articles);
        update();
      }
    } catch (e) {} finally {
      isLoading(false);
      update();
    }
  }
}
