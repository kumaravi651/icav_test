import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpController extends GetxController {
  final GlobalKey<FormState> signUpFormKey = GlobalKey<FormState>();
  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController emailController;
  TextEditingController passwordController;

  var firstName = '';
  var lastName = '';
  var email = '';
  var password = '';

  @override
  void onInit() {
    super.onInit();
    firstNameController = TextEditingController();
    lastNameController = TextEditingController();
    emailController = TextEditingController();
    passwordController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    firstNameController.dispose();
    lastNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
  }

  String validateFirstName(String value) {
    if (value.length == 0) {
      return "Please enter first name";
    }
    return null;
  }

  String validateLastName(String value) {
    if (value.length == 0) {
      return "Please enter last name";
    }
    return null;
  }

  String validateEmail(String value) {
    if (!GetUtils.isEmail(value)) {
      return "Provide valid Email";
    }
    return null;
  }

  String validatePassword(String value) {
    if (value.length < 6) {
      return "Password must be of 6 characters";
    }
    return null;
  }

  void checkSignUp() {
    final isValid = signUpFormKey.currentState?.validate();
    if (!isValid) {
      return;
    }
    signUpFormKey.currentState?.save();
  }
}
