abstract class Routes {
  static const SPLASHSCREEN = Paths.SPLASHSCREEN;
  static const HOME = Paths.HOME;
  static const LOGIN = Paths.LOGIN;
  static const SIGNUP = Paths.SIGNUP;
}

abstract class Paths {
  static const SPLASHSCREEN = '/splashscreen';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
}
