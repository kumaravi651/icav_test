import 'package:get/get.dart';
import 'package:icavtech/bindings/home_bindings.dart';
import 'package:icavtech/bindings/login_bindings.dart';
import 'package:icavtech/bindings/sign_up_bindings.dart';
import 'package:icavtech/routes/app_routes.dart';
import 'package:icavtech/screens/home_screen.dart';
import 'package:icavtech/screens/login_screen.dart';
import 'package:icavtech/screens/sign_up_screen.dart';

class AppPages {
  String email;

  static const LOGIN = Routes.LOGIN;
  static const HOMESCREEN = Routes.HOME;

  static final routes = [
    GetPage(
      name: Paths.LOGIN,
      page: () => LoginScreen(),
      binding: LoginBindings(),
    ),
    GetPage(
      name: Paths.HOME,
      page: () => HomeScreen(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Paths.SIGNUP,
      page: () => SignUpScreen(),
      binding: SignUpBindings(),
    ),
  ];
}
