import 'package:shared_preferences/shared_preferences.dart';

class SessionManager {
  final String is_Login = "is_Login";

//set data into shared preferences like this
  Future<void> setIsLogin(bool auth_token) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(this.is_Login, auth_token);
  }

//get value from shared preferences
  Future<bool> getIsLogin() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    var check = await pref.getBool(this.is_Login) ?? false;
    return check;
  }

  Future clearSharedPrefs() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove(is_Login);
  }
}
