class User {
  final int id;
  final String email;
  final String password;

  User({this.id, this.email, this.password});

  factory User.fromDatabaseJson(Map<String, dynamic> data) {
    return User(
        id: data['id'], email: data['email'], password: data['password']);
  }

  Map<String, dynamic> toDatabaseJson() {
    return {
      "id": this.id,
      "email": this.email,
      "password": this.password,
    };
  }
}
