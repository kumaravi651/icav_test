import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/components/sign_up_component.dart';
import 'package:icavtech/controllers/sign_up_controllers.dart';

class SignUpScreen extends GetView<SignUpController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: SignUpComponent(signUpController: controller),
      ),
    );
  }
}
