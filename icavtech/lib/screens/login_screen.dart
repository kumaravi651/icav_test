import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/components/login_component.dart';
import 'package:icavtech/controllers/login_controllers.dart';

class LoginScreen extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: LoginComponent(loginController: controller),
      ),
    );
  }
}
