import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:icavtech/components/home_component.dart';
import 'package:icavtech/controllers/home_controller.dart';
import 'package:icavtech/database/database_helper.dart';
import 'package:icavtech/helper/shared_preference.dart';
import 'package:icavtech/widgets/custom_alert_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends GetView<HomeController> {
  final dbHelper = DatabaseHelper.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              var dialog = CustomAlertDialog(
                  title: "Logout",
                  message: "Are you sure, do you want to logout?",
                  onPostivePressed: () async {
                    dbHelper.deleteDB();

                    SessionManager prefs = SessionManager();
                    await prefs.clearSharedPrefs();
                    Navigator.popUntil(context,
                        (Route<dynamic> predicate) => predicate.isFirst);
                  },
                  positiveBtnText: 'Yes',
                  negativeBtnText: 'No');
              showDialog(
                  context: context, builder: (BuildContext context) => dialog);
            },
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: HomeComponent(homeController: controller),
      ),
    );
  }
}
